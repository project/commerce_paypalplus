CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Commerce PayPal PLUS module adds the PayPal PLUS payment method which 
combines the methods PayPal, Direct Debit, Credit Card or Pay Upon Invoice 
to Drupal Commerce within a single payment method. 

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_paypalplus

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_paypalplus


REQUIREMENTS
------------

This module requires the following modules:

 * Commerce (https://drupal.org/project/commerce)
 * Libraries (https://drupal.org/project/libraries)
 
This module requires the PayPal PHP Library (cf. installation):

 * PayPal PHP Library (https://github.com/paypal/PayPal-PHP-SDK/releases/download/1.13.0/PayPal-PHP-SDK-1.13.0.zip)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download the PayPal PHP Library (https://github.com/paypal/PayPal-PHP-SDK/releases/download/1.13.0/PayPal-PHP-SDK-1.13.0.zip)
   unzip it, rename the folder to paypal and place it in the sites/all/libraries
   folder within your Drupal installation


CONFIGURATION
-------------
 
  After enabling the module, visit /admin/commerce/config/payment-methods to
  enable the payment method PayPal Plus within Drupal commerce. Once it is 
  enabled, click the edit link for the payment method, go to "Actions" and click
  on the link "Enable payment method: PayPal PLUS". 

  In the settings form you have to add your PayPal API client ID and the API 
  secret. Adjust the remaining settings for your needs and finally switch
  the Server to "Live" if you are ready.

  PayPal PLUS Express Checkout relies on the cookie paypalplus_session_v2. 
  So if  you enable the option "PayPal PLUS Express Checkout", please make sure 
  that the client accepts cookies and that the cookie paypalplus_session_v2 is
  allowed. 

  IMPORTANT NOTE: Please remember that in case of PayPal Pay Upon Invoice 
  payments, it is required to put PayPal Bank Details, located under 
  Order->Payment, on the invoice issued to buyer together with legal note:

  "[Merchant name] has the claim against you in the context of an ongoing 
  factoring contract to the PayPal (Europe) S.àr.l. et Cie, S.C.A. assigned. 
  Payments with a debt-discharging effect can only be submitted to 
  PayPal (Europe) S.àr.l. et Cie, S.C.A. be done."


MAINTAINERS
-----------

Current maintainers:
 * Jürgen Haas (jurgenhaas) - https://www.drupal.org/user/168924
 * Heiko Fischer (heikofischer) - https://www.drupal.org/user/1110156
 * Kai Gertz (kgertz) - https://www.drupal.org/user/886512

This project has been sponsored by:
  PayPal - https://www.paypal.com
