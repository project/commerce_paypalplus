<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_paypalplus_default_rules_configuration() {
  $rules = array();

  $rule = rules_reaction_rule();
  $rule->label = t('PayPal PLUS Payment completed');
  $rule->tags = array('PayPal PLUS');
  $rule->active = TRUE;
  $rule
    ->event('paypalplus_payment_sale_completed')
    ->action('rules_commerce_order_status_completed', array(
      'data:select' => 'order',
    ));
  $rule->weight = -10;

  $rules['paypalplus_payment_sale_completed'] = $rule;

  return $rules;
}
