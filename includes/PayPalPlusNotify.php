<?php

use PayPal\Api\VerifyWebhookSignature;
use PayPal\Api\WebhookEvent;
use PayPal\Api\WebhookEventType;
use PayPal\Common\PayPalModel;

class PayPalPlusNotify extends PayPalPlusBase {

  /**
   * @var \PayPal\Api\WebhookEvent
   */
  protected $webhook;

  /**
   * Get a list of supported webhooks.
   *
   * @return array
   */
  protected function supportedWebhooks() {
    return array(
      'PAYMENT.SALE.PENDING',
      'PAYMENT.SALE.COMPLETED',
      'PAYMENT.SALE.DENIED',
      'PAYMENT.SALE.REFUNDED',
      'PAYMENT.SALE.REVERSED',
    );
  }

  public function validate() {
    try {
      $request_body = file_get_contents('php://input');
      $this->webhook = new WebhookEvent($request_body);

      $resource = new VerifyWebhookSignature();
      $resource->setAuthAlgo($_SERVER['HTTP_PAYPAL_AUTH_ALGO']);
      $resource->setCertUrl($_SERVER['HTTP_PAYPAL_CERT_URL']);
      $resource->setTransmissionId($_SERVER['HTTP_PAYPAL_TRANSMISSION_ID']);
      $resource->setTransmissionSig($_SERVER['HTTP_PAYPAL_TRANSMISSION_SIG']);
      $resource->setTransmissionTime($_SERVER['HTTP_PAYPAL_TRANSMISSION_TIME']);
      $resource->setRequestBody($request_body);
      $resource->setWebhookId($this->settings['webhookid']);
      $response = $resource->post($this->apiContext);
      if ($response->getVerificationStatus() == 'SUCCESS') {
        watchdog('PayPalPlus Notify', '!type - !id:<pre>!ressource</pre>', array(
          '!type' => $this->webhook->getEventType(),
          '!id' => $this->webhook->getId(),
          '!ressource' => print_r($this->webhook->getResource(), TRUE),
        ));
        return TRUE;
      }
    }
    catch (\Exception $ex) {
      $this->watchdog_set_ex($ex, t('Problem validating notification.'));
    }
    watchdog('PayPalPlus', t('Invalid notification.'), array(), WATCHDOG_ALERT);
    return FALSE;
  }

  /**
   * Trigger the event that matches the webhook.
   *
   * @return bool
   *   TRUE if we have a supporting event and a matching order.
   */
  public function execute() {
    $key = $this->getKeyFromName($this->webhook->getEventType());
    $resource = $this->webhook->getResource();
    if ($resource instanceof PayPalModel) {
      $order = $this->getOrderFromSale($resource->id);
    }

    if (!empty($order)) {
      rules_invoke_all($key, $order);
      return TRUE;
    }

    watchdog('PayPalPlus Notify', '!type - !id: no matching order found!', array(
      '!type' => $this->webhook->getEventType(),
      '!id' => $this->webhook->getId(),
    ), WATCHDOG_ALERT);
    return FALSE;
  }

  public function updateEvents() {
    $events = array();

    // Check all available event types from the API and make sure we have a matching rules event.
    foreach (WebhookEventType::availableEventTypes($this->apiContext)->getEventTypes() as $et) {
      if ($et->getStatus() !== 'ENABLED') {
        continue;
      }
      if (!in_array($et->getName(), $this->supportedWebhooks())){
        continue;
      }
      $events[$this->getKeyFromName($et->getName())] = array(
        'label' => $et->getDescription(),
        'group' => t('PayPal PLUS'),
        'variables' => array(
          'order' => array(
            'type' => 'commerce_order',
            'label' => t('The order object'),
          ),
        ),
      );
    }

    return $events;
  }

  /**
   * @param string $name
   *
   * @return string
   */
  protected function getKeyFromName($name) {
    return 'paypalplus_' . strtolower(str_replace('.', '_', $name));
  }

  /**
   * @param string $id
   *
   * @return bool|stdClass
   */
  protected function getOrderFromSale($id) {
    foreach (commerce_payment_transaction_load_multiple(array(), array('remote_id' => $id)) as $transaction) {
      $order = commerce_order_load($transaction->order_id);
      if ($order) {
        return $order;
      }
    }
    return FALSE;
  }

}
