<?php

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Presentation;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Transaction;
use PayPal\Api\Webhook;
use PayPal\Api\WebhookEventType;
use PayPal\Api\WebProfile;

/**
 * PayPal Plus API Client.
 */
class PayPalPlusClient extends PayPalPlusBase {

  const SCRIPT_FILE = 'https://www.paypalobjects.com/webstatic/ppplus/ppplus.min.js';
  const EXPRESS_SCRIPT_FILE = 'https://www.paypalobjects.com/api/checkout.js';

  /**
   * The payment object from PayPal.
   *
   * @var Payment
   */
  protected $payment;

  /**
   * The order object which should be paid for.
   *
   * @var stdClass
   */
  protected $order;

  /**
   * PayPalPlus constructor.
   *
   * @param array $settings
   *   The settings of the payment method.
   * @param stdClass $order
   *   The order object which should be paid for.
   */
  public function __construct($settings, $order) {
    parent::__construct($settings);
    $this->order = $order;
  }

  public function verifyExperienceProfile() {
    if (!empty($this->settings['experience_profile']['id'])) {
      try {
        $profile = WebProfile::get($this->settings['experience_profile']['id'], $this->apiContext);
        $presentation = $profile->getPresentation();
        $inputfields = $profile->getInputFields();
        $isNew = FALSE;
      }
      catch (\Exception $ex) {
        // Ignore this, we will create a new one.
      }
    }

    if (empty($profile)) {
      $profile = new WebProfile();
      // We use the timestamp to avoid issues with non-unique profile names.
      $profile->setName('default-' . REQUEST_TIME);
      $isNew = TRUE;
    }
    if (empty($presentation)) {
      $presentation = new Presentation();
    }
    if (empty($inputfields)) {
      $inputfields = new InputFields();
    }

    $changed = FALSE;

    if ($presentation->getBrandName() != $this->settings['experience_profile']['brand_name']) {
      $presentation->setBrandName($this->settings['experience_profile']['brand_name']);
      $changed = TRUE;
    }
    if ($presentation->getLogoImage() != $this->settings['experience_profile']['logo_image']) {
      if (empty($this->settings['experience_profile']['logo_image'])) {
        $presentation->setLogoImage(NULL);
      }
      else {
        $presentation->setLogoImage($this->settings['experience_profile']['logo_image']);
      }
      $changed = TRUE;
    }
    if ($presentation->getLocaleCode() != $this->settings['experience_profile']['locale_code']) {
      $presentation->setLocaleCode($this->settings['experience_profile']['locale_code']);
      $changed = TRUE;
    }

    if ($inputfields->getAllowNote() != $this->settings['experience_profile']['allow_note']) {
      $inputfields->setAllowNote((int) $this->settings['experience_profile']['allow_note']);
      $changed = TRUE;
    }
    if ($inputfields->getNoShipping() != $this->settings['experience_profile']['no_shipping']) {
      $inputfields->setNoShipping((int) $this->settings['experience_profile']['no_shipping']);
      $changed = TRUE;
    }
    if ($inputfields->getAddressOverride() != $this->settings['experience_profile']['address_override']) {
      $inputfields->setAddressOverride((int) $this->settings['experience_profile']['address_override']);
      $changed = TRUE;
    }
    $profile->setPresentation($presentation);
    $profile->setInputFields($inputfields);

    try {
      if ($isNew) {
        $profile = $profile->create($this->apiContext);
      }
      else if ($changed) {
        $profile->update($this->apiContext);
      }
    }
    catch (\Exception $ex) {
      // set watchdog information for the merchant
      $this->watchdog_set_ex($ex, t('PayPal PLUS experience profile could not be created/updated.'));
      // set information message for the customer
      drupal_set_message(t('PayPal experience profile could not be created/updated.'), 'error');
      return FALSE;
    }
    $this->settings['experience_profile']['id'] = $profile->getId();
    return $profile->getId();
  }

  public function registerWebhook() {
    $webhookId = $this->settings['webhookid'][$this->settings['server']];
    $webhookUrl = url('commerce_paypalplus/notify', array('absolute' => TRUE));

    if (parse_url($webhookUrl, PHP_URL_SCHEME) !== 'https') {
      drupal_set_message(t('In order to use webhook notifications you need to serve your site over https.'), 'warning');
      return NULL;
    }

    if (!empty($webhookId)) {
      try {
        $webhook = Webhook::get($webhookId, $this->apiContext);
        if (!empty($webhook) && $webhookUrl != $webhook->getUrl()) {
          $patch = new Patch();
          $patch->setOp('replace')
            ->setPath('/url')
            ->setValue($webhookUrl);
          $patchRequest = new PatchRequest();
          $patchRequest->addPatch($patch);
          $webhook->update($patchRequest, $this->apiContext);
        }
      }
      catch (\Exception $ex) {
        $webhookId = '';
      }
    }

    // Try to get the webhookID, perhaps a webhook is already established but the ID is not saved in settings yet.
    if (empty($webhookId)) {
      $webhook = Webhook::getAllWithParams(array(), $this->apiContext);
      if (!empty($webhook)) {
        foreach ($webhook->getWebhooks() as $webhookObject) {
          if ($webhookUrl == $webhookObject->getUrl()) {
            $webhookId = $webhookObject->getId();
          }
        }
      }
    }

    if (empty($webhookId)) {
      try {
        // Create a new webhook
        $webhook = new Webhook();
        $webhook->setUrl($webhookUrl);
        $eventType = new WebhookEventType();
        $eventType->setName('*');
        $webhook->setEventTypes([$eventType]);
        $webhook = $webhook->create($this->apiContext);
        $webhookId = $webhook->getId();
      }
      catch (\Exception $ex) {
        // set watchdog information for the merchant
        $this->watchdog_set_ex($ex, t('PayPal PLUS webhook could not be created/updated.'));
        // set information message for the merchant
        drupal_set_message(t('PayPal PLUS webhook could not be created/updated.'), 'error');
        return FALSE;
      }
    }
    return $webhookId;
  }

  public function isReturning() {
    return
      isset($_GET['paymentId']) &&
      isset($_GET['PayerID']) &&
      isset($this->order->data['paypalplus']['id']) &&
      $_GET['paymentId'] == $this->order->data['paypalplus']['id'];
  }

  /**
   * Determine if PayPal PLUS is configured correctly.
   *
   * This sets an error message for the user in the UI in case that
   * PayPal PLUS can't be used at this stage.
   *
   * @return bool
   *   TRUE, if the API is configured correctly, FALSE otherwise.
   */
  public function available() {
    if (!$this->available) {
      drupal_set_message(t('PayPal PLUS is not configured for use. Please contact an administrator to resolve this issue.'), 'error');
    }
    else {
      // If we've already created the payment object for the current
      // order, let's get that back from PayPal.
      if (empty($this->payment) && isset($this->order->data['paypalplus']['id'])) {
        try {
          $this->payment = Payment::get($this->order->data['paypalplus']['id'], $this->apiContext);
        }
        catch (\Exception $ex) {
          // Ignore this and simply create a new payment.
        }
      }
    }
    return $this->available;
  }

  /**
   * Returns the approval URL from the PayPal payment object if available.
   *
   * @return bool|string
   *   The approval URL to which you can redirect or FALSE if not available.
   */
  public function getApprovalUrl() {
    return empty($this->payment) ? FALSE : $this->payment->getApprovalLink();
  }

  /**
   * Returns the payer object from the PayPal payment object if available.
   *
   * @return bool|PayerInfo
   *   The payer or FALSE if not available.
   */
  public function getPayerInfo() {
    $payer = $this->available() ? $this->payment->getPayer() : FALSE;
    return empty($payer) ? FALSE : $payer->getPayerInfo();
  }

  /**
   * Returns the country for the PayPal process from billing or shipping address or falls back to country from settings.
   *
   * @return string
   *   The country code.
   */
  protected function getCountry() {
    foreach (array('billing', 'shipping') as $type) {
      $profile_field = 'commerce_customer_' . $type;
      if (isset($this->order->{$profile_field}[LANGUAGE_NONE][0]['profile_id'])) {
        $profile = commerce_customer_profile_load($this->order->{$profile_field}[LANGUAGE_NONE][0]['profile_id']);
        $pa = $profile->commerce_customer_address[LANGUAGE_NONE][0];
        return $pa['country'];
      }
    }
    return $this->settings['country'];
  }

  /**
   * Callback to save the PayPal payment objects with the current order.
   */
  protected function savePayment() {
    $this->order->data['paypalplus']['id'] = $this->payment->getId();
    commerce_order_save($this->order);
  }

  /**
   * Callback to flag the current order for PayPal Express being used or not.
   *
   * @param bool $flag
   */
  public function setOrderExpressStatus($flag) {
    if (!isset($this->order->data['paypalplus']['express']) || (bool) $this->order->data['paypalplus']['express'] !== (bool) $flag) {
      $this->order->data['paypalplus']['express'] = $flag;
      commerce_order_save($this->order);
    }
  }

  /**
   * Store the payer ID and payment token with the order.
   *
   * @param string $payerID
   * @param string $paymentToken
   */
  public function setOrderDetails($payerID, $paymentToken) {
    $this->order->data['paypalplus']['token'] = $paymentToken;
    $this->order->data['paypalplus']['payerid'] = $payerID;
    commerce_order_save($this->order);
  }

  /**
   * Return the payer's email address.
   *
   * @return string|bool
   *   The email address of the payer or FALSE if we can't get the details.
   */
  public function getPayerEmail() {
    $payer = $this->getPayerInfo();
    if ($payer) {
      return $payer->getEmail();
    }
    return FALSE;
  }

  /**
   * Check, if address profile already exists for PayPal Express Checkout.
   *
   * @param PayerInfo $payer
   *  The payer object.
   * @param ShippingAddress $paypal_address
   *  The address, returned from PayPal express checkout validation
   * @param string $type
   *  Address type billing or shipping.
   * @return stdClass|bool
   *  The loaded address profile or false, when no matching address is found.
   */
  protected function checkProfile($payer, $paypal_address, $type) {
    // Create array with address information from PayPal EC
    $pp_address = array(
      'name_line' => $paypal_address->getRecipientName(),
      'country' => $paypal_address->getCountryCode(),
      'locality' => $paypal_address->getCity(),
      'administrative_area' => $paypal_address->getState(),
      'postal_code' => $paypal_address->getPostalCode(),
      'thoroughfare' => $paypal_address->getLine1(),
      'premise' => $paypal_address->getLine2(),
    );

    // Identify all available address profiles of the buyer.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_customer_profile')
      ->propertyCondition('uid', $this->order->uid)
      ->propertyCondition('type', $type);

    $results = $query->execute();

    if (!empty($results['commerce_customer_profile'])) {
      // Load all available address profiles.
      $profiles = commerce_customer_profile_load_multiple(array_keys($results['commerce_customer_profile']));

      // Compare loaded buyer address profile with PayPal address
      foreach ($profiles as $profile) {
        $commerce_address = $profile->commerce_customer_address[LANGUAGE_NONE][0];
        foreach ($pp_address as $key => $value) {
          if (empty($value) && empty($commerce_address[$key])) {
            // Both values are somehow empty and we treat that as equal, going to the next value.
            continue;
          }
          if ($commerce_address[$key] != $value) {
            // This profile doesn't match, continue with next one.
            continue 2;
          }
        }
        return $profile;
      }
    }
    // No address profile found, the address must be created in a later step.
    return FALSE;
  }

  /**
   * Create a customer profile with data from PayPal.
   *
   * @param string $type
   */
  public function ensureProfile($type) {
    $payer = $this->getPayerInfo();
    if ($payer) {
      /** @var ShippingAddress $paypal_address */
      $paypal_address = $payer->getShippingAddress();
      if ($paypal_address) {
        // Check, if address profile already exists.
        $customer_profile = $this->checkProfile($payer, $paypal_address, $type);

        if (!$customer_profile) {
          // Create a new address profile for this order.
          $customer_profile = commerce_customer_profile_new($type, $this->order->uid);

          $customer_profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $customer_profile);
          $customer_profile_wrapper->commerce_customer_address->name_line = $paypal_address->getRecipientName();
          $customer_profile_wrapper->commerce_customer_address->country = $paypal_address->getCountryCode();
          $customer_profile_wrapper->commerce_customer_address->locality = $paypal_address->getCity();
          $customer_profile_wrapper->commerce_customer_address->administrative_area = $paypal_address->getState();
          $customer_profile_wrapper->commerce_customer_address->postal_code = $paypal_address->getPostalCode();
          $customer_profile_wrapper->commerce_customer_address->thoroughfare = $paypal_address->getLine1();
          $customer_profile_wrapper->commerce_customer_address->premise = $paypal_address->getLine2();
          $customer_profile->commerce_customer_address[LANGUAGE_NONE][0]['data'] = serialize($paypal_address);

          // Save the customer profile.
          commerce_customer_profile_save($customer_profile);
        }

        $profile_field = 'commerce_customer_' . $type;
        $this->order->{$profile_field}[LANGUAGE_NONE][0]['profile_id'] = $customer_profile->profile_id;
        commerce_order_save($this->order);
      }
    }
  }

  /**
   * Formats a price amount into a decimal value as expected by PayPal.
   *
   * @param int $amount
   *   An integer price amount in cents.
   * @param string $currency_code
   *   The currency code of the price.
   *
   * @return string
   *  The decimal price amount as expected by PayPal API servers.
   */
  protected function priceAmount($amount, $currency_code) {
    $rounded_amount = commerce_currency_round($amount, commerce_currency_load($currency_code));
    return number_format(commerce_currency_amount_to_decimal($rounded_amount, $currency_code), 2, '.', '');
  }

  /**
   * Determins a valid currency for the current order.
   *
   * @param EntityDrupalWrapper $wrapper
   *   The order wrapper which contains metadata about the current order.
   *
   * @return string
   *   The currency code which is accepted by the PayPal API and matches best
   *   what's configured with the order.
   */
  protected function currency($wrapper) {
    $currency_code = $this->settings['currency_code'];
    /** @noinspection PhpUndefinedFieldInspection */
    $order_currency_code = $wrapper->commerce_order_total->currency_code->value();

    if (!empty($this->settings['allow_supported_currencies']) && in_array($order_currency_code, array_keys(commerce_paypalplus_currencies()))) {
      $currency_code = $order_currency_code;
    }
    return $currency_code;
  }

  /**
   * Calculate item list, total amount and currency used by create and update callbacks for the payment objects.
   *
   * @return array
   */
  protected function calculateItemListAndTotal() {
    /** @var EntityDrupalWrapper $wrapper */
    $wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    $itemList = new ItemList();
    $subtotalAmount = 0;
    $shippingAmount = 0;
    $discountAmount = 0;
    $taxAmount      = 0;
    $feeAmount      = 0;
    $currency = $this->currency($wrapper);
    /** @var EntityDrupalWrapper $item */
    /** @noinspection PhpUndefinedFieldInspection */
    foreach ($wrapper->commerce_line_items as $item) {
      $line_item = $item->value();
      $line_item_amount = $line_item->commerce_unit_price[LANGUAGE_NONE][0]['amount'];
      $line_item_currency = $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code'];
      $line_item_quantity = (int) $line_item->quantity;
      if (function_exists('commerce_tax_total_amount')) {
        $line_item_components = $line_item->commerce_unit_price[LANGUAGE_NONE][0]['data']['components'];
        $line_item_tax = commerce_tax_total_amount($line_item_components, FALSE, $line_item_currency);
        $taxAmount += $line_item_tax * $line_item_quantity;
      }
      $line_item_amount_converted = commerce_currency_convert($line_item_amount, $line_item_currency, $currency);
      $amount = $this->priceAmount($line_item_amount_converted, $currency);

      switch ($line_item->type) {
        case 'shipping':
          $shippingAmount += $line_item_amount_converted * $line_item_quantity;
          break;

        case 'commerce_discount':
        case 'product_discount':
          $discountAmount += $line_item_amount_converted * $line_item_quantity;
          break;

        case 'exactor_calc':
          $taxAmount += $line_item_amount_converted * $line_item_quantity;
          break;

        case 'product':
          $subtotalAmount += $line_item_amount_converted * $line_item_quantity;
          $item = new Item();
          $item->setName($line_item->line_item_label)
            ->setCurrency($currency)
            ->setQuantity($line_item_quantity)
            ->setPrice($amount);
          $itemList->addItem($item);
          break;

        default:
          $feeAmount += $line_item_amount_converted * $line_item_quantity;
      }
    }

    $totalAmount = $subtotalAmount + $shippingAmount + $discountAmount + $taxAmount + $feeAmount;
    $details = new Details();
    $details
      ->setSubtotal($this->priceAmount($subtotalAmount, $currency))
      ->setShipping($this->priceAmount($shippingAmount, $currency))
      ->setShippingDiscount($this->priceAmount($discountAmount, $currency))
      ->setTax($this->priceAmount($taxAmount, $currency))
      ->setFee($this->priceAmount($feeAmount, $currency));
    return array($itemList, $totalAmount, $currency, $details, $subtotalAmount);
  }

  /**
   * Creates a new PayPal PLUS payment through their API and stored on their servers.
   *
   * Best practice:
   * - For PayPal PLUS line items must be submitted
   * - Shipping / billing address as a best practice should not be submitted here but
   *   later by using PATCH Payment
   *
   * @param bool $force
   *   Force to create a new payment object even if the amount hasn't changed. Used by expressCreate().
   *
   * @return bool
   */
  public function create($force = FALSE) {
    list($itemList, $totalAmount, $currency, $details, $subtotalAmount) = $this->calculateItemListAndTotal();
    if (!$force && !empty($this->payment)) {
      $subTotalPayment = $this->payment->getTransactions()[0]->getAmount()->getDetails()->getSubtotal();
      $subTotelCurrent = $this->priceAmount($subtotalAmount, $currency);
      if ($subTotalPayment == $subTotelCurrent) {
        return TRUE;
      }
    }

    $payer = new Payer();
    $payer->setPaymentMethod('paypal');

    $amount = new Amount();
    $amount->setCurrency($currency)
      ->setTotal($this->priceAmount($totalAmount, $currency))
      ->setDetails($details);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
      ->setItemList($itemList);

    if (empty($this->order->data['payment_redirect_key'])) {
      // Generate a payment redirect key.
      $this->order->data['payment_redirect_key'] = drupal_hash_base64(time());
    }
    $redirectSuccess = url('checkout/' . $this->order->order_id . '/payment/return/' . $this->order->data['payment_redirect_key'], array('absolute' => TRUE));
    $redirectCancel = url('checkout/' . $this->order->order_id . '/payment/back/' . $this->order->data['payment_redirect_key'], array('absolute' => TRUE));
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl($redirectSuccess)
      ->setCancelUrl($redirectCancel);

    $this->payment = new Payment();
    $this->payment->setIntent('sale')
      ->setExperienceProfileId($this->settings['experience_profile']['id'])
      ->setPayer($payer)
      ->setRedirectUrls($redirectUrls)
      ->setTransactions([$transaction]);

    try {
      $this->payment->create($this->apiContext);
      $this->savePayment();
    } catch (\Exception $ex) {
      // set watchdog information for the merchant
      $this->watchdog_set_ex($ex, t('PayPal PLUS payment object could not be created.'));
      // set information message for the customer
      drupal_set_message(t('PayPal PLUS payment could not be created, please choose another payment method.'), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Creates a new PayPal PLUS payment through their API and returns payment ID as JSON to calling Ajax request.
   */
  public function createExpress() {
    if ($this->create(TRUE)) {
      $this->setOrderExpressStatus(TRUE);
      drupal_json_output(array(
        'paymentID' => $this->payment->getId(),
      ));
      drupal_exit();
    }
  }

  /**
   * Patches the PayPal PLUS payment object.
   *
   * Best practice:
   * - Lines items cannot be update at any stage.
   * - Billing address can only be added/updated prior to the PayPal hosted pages.
   * - Shipping costs, discounts and other fees can be updated as part of the amount object
   */
  public function update() {
    if (empty($this->payment)) {
      // set watchdog information for the merchant, this shouldn't happen.
      watchdog('PayPalPlus', t('Payment object is empty on update call, shipping and billing address could not be patched.'), array(), WATCHDOG_ERROR);
      drupal_set_message(t('PayPal PLUS payment not available, please try again later.'), 'error');
      return FALSE;
    }

    $patches = 0;
    $patchRequest = new PatchRequest();

    // Check if we have to patch the total amount of the payment object.
    list(, $totalAmount, $currency, $details, ) = $this->calculateItemListAndTotal();
    $amount = $this->payment->getTransactions()[0]->getAmount();
    $price = $this->priceAmount($totalAmount, $currency);
    if ($amount->getTotal() != $price || $amount->getCurrency() != $currency) {
      $amount->setCurrency($currency)
        ->setTotal($price)
        ->setDetails($details);
      $amountPatch = new Patch();
      $amountPatch
        ->setOp('replace')
        ->setPath('/transactions/0/amount')
        ->setValue($amount);
      $patchRequest->addPatch($amountPatch);
      $patches++;
    }

    // Only patch addresses if not in Express mode.
    if (empty($this->order->data['paypalplus']['express'])) {

      if (isset($this->order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id'])) {
        $profile = commerce_customer_profile_load($this->order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);
        $pa = $profile->commerce_customer_address[LANGUAGE_NONE][0];
        $shippingAddress = new ShippingAddress(array(
          'recipient_name' => $pa['name_line'],
        ));
        $shippingAddress
          ->setCountryCode($pa['country'])
          ->setState($pa['administrative_area'])
          ->setCity($pa['locality'])
          ->setPostalCode($pa['postal_code'])
          ->setLine1($pa['thoroughfare'])
          ->setLine2($pa['premise']);
        $shippingPatch = new Patch();
        $shippingPatch
          ->setOp('add')
          ->setPath('/transactions/0/item_list/shipping_address')
          ->setValue($shippingAddress);
        $patchRequest->addPatch($shippingPatch);
        $patches++;
      }

      if (isset($this->order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'])) {
        $profile = commerce_customer_profile_load($this->order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id']);
        $pa = $profile->commerce_customer_address[LANGUAGE_NONE][0];
        $billingAddress = new ShippingAddress();
        $billingAddress
          ->setCountryCode($pa['country'])
          ->setState($pa['administrative_area'])
          ->setCity($pa['locality'])
          ->setPostalCode($pa['postal_code'])
          ->setLine1($pa['thoroughfare'])
          ->setLine2($pa['premise']);
        $billingPatch = new Patch();
        $billingPatch
          ->setOp('add')
          ->setPath('/potential_payer_info/billing_address')
          ->setValue($billingAddress);
        $patchRequest->addPatch($billingPatch);
        $patches++;
      }

    }

    if ($patches > 0) {
      try {
        if (!$this->payment->update($patchRequest, $this->apiContext)) {
          watchdog('PayPalPlus', t('Shipping and billing address could not be patched.'), array(), WATCHDOG_ERROR);
          drupal_set_message(t('PayPal PLUS payment not available, please try again later.'), 'error');
          return FALSE;
        }
      } catch (\Exception $ex) {
        // set watchdog information for the merchant
        $this->watchdog_set_ex($ex, t('PayPal PLUS error while updating shipping and billing address.'));
        // set information message for the customer
        drupal_set_message(t('PayPal PLUS payment not available, please try again later.'), 'error');
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Finally executes the PayPal PLUS payment object.
   */
  public function execute() {
    $state = $this->payment->getState();

    if ($state == 'approved') {
      return TRUE;
    }

    if ($state == 'created') {

      // Execute payment with PayPal.
      $execution = new PaymentExecution();
      $execution->setPayerId($this->order->data['paypalplus']['payerid']);
      try {
        $result = $this->payment->execute($execution, $this->apiContext);
        if ($result->getState() == 'approved') {
          $payment_method = commerce_payment_method_instance_load($this->order->data['payment_method']);
          $amount = $this->payment->getTransactions()[0]->getAmount();

          // Create a new payment transaction for the order.
          $transaction = commerce_payment_transaction_new('paypalplus', $this->order->order_id);
          $transaction->instance_id = $payment_method['instance_id'];
          $transaction->remote_id = $this->payment->getTransactions()[0]->getRelatedResources()[0]->getSale()->getId();
          $transaction->amount = commerce_currency_decimal_to_amount($amount->getTotal(), $amount->getCurrency());
          $transaction->currency_code = $amount->getCurrency();
          $transaction->payload[REQUEST_TIME . '-paypalplus'] = $result;
          $transaction->remote_status = 'approved';
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          $transaction->message = t('The payment has completed.');

          // Save the transaction information.
          commerce_payment_transaction_save($transaction);
        }
      }
      catch (\Exception $ex) {
        // set watchdog information for the merchant
        $this->watchdog_set_ex($ex, t('PayPal PLUS payment could not be executed.'));
        // set information message for the customer
        drupal_set_message(t('PayPal PLUS payment could not be executed.'), 'error');
        return FALSE;
      }
    }
    else {
      watchdog('PayPalPlus', t('Payment could not be executed because it is not created.'), array(), WATCHDOG_ERROR);
      drupal_set_message(t('PayPal PLUS payment not available, please try again later.'), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Provides the correct markup for the checkout page which includes the PayPal PLUS panel into the site.
   *
   * @return string
   *   The markup ready to be included.
   */
  public function checkoutMarkup() {
    return '<div id="ppp-placeholder"> </div>';
  }

  /**
   * Provides the correct markup for the cart to provide Express Checkout.
   *
   * @return string
   *   The markup ready to be included.
   */
  public function expressMarkup() {
    return '<div id="ppp-express"> </div>';
  }

  /**
   * Provides the custom script for the markup on the checkout page which includes the PayPal PLUS panel into the site.
   *
   * @return string
   *   The script ready to be included.
   */
  public function checkoutScript() {
    global $language;
    return '
        var ppp = PAYPAL.apps.PPP({
          "approvalUrl": "' . $this->getApprovalUrl() . '",
          "placeholder": "ppp-placeholder",
          "mode": "' . $this->settings['server'] . '",
          "country": "' . $this->getCountry() . '",
          "language": "' . strtoupper($language->language) . '",
          "buttonLocation": "outside",
          "disableContinue": "continueButton",
          "enableContinue": "continueButton",
          "showPuiOnSandbox": true,
          "showLoadingIndicator": true,
          "useraction": "commit",
          "thirdPartyPaymentMethods": ' . $this->thirdPartyConfigurations() . ',
          "onThirdPartyPaymentMethodSelected": function(data){;Drupal.commerce_paypalplus.selectThirdPartyPaymentMethod(data, "continueButton");},
          "onThirdPartyPaymentMethodDeselected": function(data){Drupal.commerce_paypalplus.deselectThirdPartyPaymentMethod(data, "continueButton");},
          "onContinue": function(){}
        });';
  }

  /**
   * Provides the custom script for the markup on the cart to provide Express Checkout.
   *
   * @return string
   *   The script ready to be included.
   */
  public function expressScript($formid, $submitop) {
    // When using ECS, "env" can be sandbox or production.
    // (Remark: when using PPP, "mode" can be sandbox or live and $this->settings['server'] can be used directly.)
    return '
        paypal.Button.render({
          "env": "' . ($this->settings['server'] == 'live' ? 'production' : 'sandbox') . '",
          "locale": "' . commerce_paypalplus_get_locale() . '",
          "commit": false,
          "payment": function() {
            var CREATE_URL = "' . url('commerce_paypalplus/' . $this->order->order_id . '/express/create/' . drupal_get_token('paypalplus-express-create-' . $this->order->order_id)) . '";
            return paypal.request.post(CREATE_URL).then(function(res) {
              return res.paymentID;
            });
          },
          "onAuthorize": function(data, actions) {
            var SAVE_URL = "' . url('commerce_paypalplus/' . $this->order->order_id . '/express/save/' . drupal_get_token('paypalplus-express-save-' . $this->order->order_id)) . '";
            return paypal.request.post(SAVE_URL, data).then(function(res) {
              form = document.getElementById("' . $formid . '");
              opValue = document.createElement("input");
              opValue.setAttribute("type", "hidden");
              opValue.setAttribute("name", "op");
              opValue.setAttribute("value", "' . $submitop . '");
              form.appendChild(opValue);
              form.submit();
            });
          },
          "onCancel": function(data, actions) {
            var CANCEL_URL = "' . url('commerce_paypalplus/' . $this->order->order_id . '/express/cancel/' . drupal_get_token('paypalplus-express-cancel-or-error-' . $this->order->order_id)) . '";
            return paypal.request.post(CANCEL_URL);
          },
          "onError": function(err) {
            var ERROR_URL = "' . url('commerce_paypalplus/' . $this->order->order_id . '/express/error/' . drupal_get_token('paypalplus-express-cancel-or-error-' . $this->order->order_id)) . '";
            return paypal.request.post(ERROR_URL);
          }
        }, "#ppp-express");';
  }

  /**
   * Provides the correct markup for the redirect page which redirects to PayPal PLUS.
   *
   * @return string
   *   The markup ready to be included.
   */
  public function redirectMarkup() {
    return '
      <button
        type="button"
        id="paypalplus-redirect"
        onclick="PAYPAL.apps.PPP.doCheckout();">' .
        t('Proceed to PayPal PLUS') .
      '</button>';
  }
/**
 * Returns an array of the currently active payment methods
 *
 * @return array the active payment methods
 */
  public function availablePaymentMethods() {
    $result = array();
    $active_payment_rules = db_select('rules_config', 'rc')
      ->fields('rc', array('id', 'name'))
      ->condition('rc.module', 'commerce_payment')
      ->condition('rc.active', 1)
      ->execute()
      ->fetchAllKeyed(0,1);

    foreach ($active_payment_rules as $rule_name) {
      // Cut "commerce_payment_" to get the id
      $id = substr($rule_name, 17);
      $method = commerce_payment_method_instance_load($id . '|' .$rule_name);
      $result[$rule_name] = $method;
    }
    return $result;
  }

  /**
   * Returns third party payment methods as expected by the paypal library as JSON config
   *
   * @return string the JSON config object with third party paymemt methods
   */
  protected function thirdPartyConfigurations() {
    global $base_url;
    $result = '';
    $method_configs = array();
    $payment_methods = $this->availablePaymentMethods();
    // create code for the configuration JSON object in checkoutScript()
    foreach($payment_methods as $rule_name => $method) { 
      if ($method['base'] !== 'commerce_paypalplus') {
        $method_configs[] = array(
          'methodName' => $method['display_title'], 
          'redirectUrl' => 'https://www.paypal.com', // dummy entry, not really needed here
          // TODO: what would be the best option fror embedding icons for third party methods?
          // for now, let's just display the relatively neutral pui logo from paypal
          //'imageUrl' => $base_url . '/' . drupal_get_path('module', 'commerce_paypalplus') . '/img/money.png',
          'imageUrl' => "https://www.paypalobjects.com/webstatic/ppplus/images/pui-logo.png",
          'description' => $method['description'],
        );
      }
    }
    $result = json_encode($method_configs);
    return $result;
  }
}
