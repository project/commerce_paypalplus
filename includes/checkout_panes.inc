<?php

use PayPal\Api\Payment;

function commerce_paypalplus_transaction_detail_defaults($type) {
  if (filter_format_load('filtered_html')) {
    $format = 'filtered_html';
  }
  else {
    $format = filter_fallback_format();
  }

  switch ($type) {
    case 'pay_upon_invoice':
      $desc = array();
      $desc[] = t('[site:name] has the claim against you in the context of an ongoing factoring contract to the PayPal (Europe) S.àr.l. et Cie, S.C.A. assigned. Payments with a debt-discharging effect can only be submitted to PayPal (Europe) S.àr.l. et Cie, S.C.A. be done.');
      $desc[] = t('Please wire transfer the amount [paypalplus_payment_instruction:amount] to the following bank account and use the reference number [paypalplus_payment_instruction:reference_number]:');
      $bank_details = array();
      $bank_details[] = t('Bank name: [paypalplus_payment_instruction:bank_name]');
      $bank_details[] = t('Account holder name: [paypalplus_payment_instruction:bank_account_holder_name]');
      $bank_details[] = t('IBAN: [paypalplus_payment_instruction:bank_iban]');
      $bank_details[] = t('BIC: [paypalplus_payment_instruction:bank_bic]');

      if (filter_format_load('filtered_html')) {
        $value = implode("\n\n", $desc) . "\n\n" . implode("<br/>", $bank_details);
      } else {
        $value = implode("\n", $desc) . "\n" . implode("\n", $bank_details);
      }

      break;

    default:
      $value = '';
  }

  return array('value' => $value, 'format' => $format);
}

/**
 * Checkout pane settings form.
 */
function commerce_paypalplus_pane_completed_payment_settings_form($checkout_pane) {
  $form = array();

  $form['container'] = array(
    '#type' => 'container',
  );

  $types = array(
    'pay_upon_invoice' => t('Pay upon invoice'),
  );
  foreach ($types as $type => $title) {
    $message = variable_get('commerce_paypalplus_transaction_detail_' . $type, commerce_paypalplus_transaction_detail_defaults($type));

    $form['container']['commerce_paypalplus_transaction_detail_' . $type] = array(
      '#type' => 'text_format',
      '#title' => $title,
      '#default_value' => $message['value'],
      '#format' => $message['format'],
      '#access' => filter_access(filter_format_load($message['format'])),
    );
  }

  $var_info = array(
    'site' => array(
      'type' => 'site',
      'label' => t('Site information'),
      'description' => t('Site-wide settings and other global information.'),
    ),
    'paypalplus_payment_instruction' => array(
      'label' => t('Payment instructions'),
      'type' => 'paypalplus_payment_instruction',
    ),
    'commerce_order' => array(
      'label' => t('Order'),
      'type' => 'commerce_order',
    ),
  );

  $form['container']['commerce_paypalplus_transaction_detail_help'] = RulesTokenEvaluator::help($var_info);

  return $form;
}

/**
 * Checkout pane callback.
 */
function commerce_paypalplus_pane_completed_payment_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Make sure the PayPal SDK is being loaded such that the classes can be auto-loaded.
  commerce_paypalplus_api_client(array());

  $pane_form = array();

  foreach (commerce_payment_transaction_load_multiple(array(), array('order_id' => $order->order_id)) as $transaction) {
    /** @var Payment $payload */
    foreach ($transaction->payload as $payload) {
      $instruction = $payload->getPaymentInstruction();
      if (!empty($instruction)) {
        $type = strtolower($instruction->getInstructionType());
        $key = 'commerce_paypalplus_transaction_detail_' . $type;
        if ($message = variable_get($key, commerce_paypalplus_transaction_detail_defaults($type))) {
          // Perform translation.
          $message['value'] = commerce_i18n_string(str_replace('_', ':', $key), $message['value'], array('sanitize' => FALSE));
          // Perform token replacement.
          $message['value'] = token_replace($message['value'], array(
            'commerce-order' => $order,
            'paypalplus_payment_instruction' => $payload->getPaymentInstruction(),
          ), array('clear' => TRUE));
          // Apply the proper text format.
          $message['value'] = check_markup($message['value'], $message['format']);

          $pane_form[$key] = array(
            '#markup' => '<div class="' . str_replace('_', '-', $key) . '">' . $message['value'] . '</div>',
          );

          if (module_exists('commerce_message')) {
            $commerce_message = message_create('commerce_order_admin_comment', array('uid' => $order->uid));
            $wrapper = entity_metadata_wrapper('message', $commerce_message);
            $wrapper->message_commerce_order->set($order);
            $wrapper->message_commerce_body->set($message);
            $wrapper->save();
          }

        }
      }
    }
  }

  return $pane_form;
}
