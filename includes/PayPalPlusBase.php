<?php

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

abstract class PayPalPlusBase {

  /**
   * The settings of the payment method.
   *
   * @var array
   */
  protected $settings;

  /**
   * The API context for the REST API.
   *
   * @var ApiContext
   */
  protected $apiContext;

  /**
   * Flag which indicates if the PayPal client is available and setup correctly.
   *
   * @var bool
   */
  protected $available = FALSE;

  /**
   * PayPalPlus constructor.
   *
   * @param array $settings
   *   The settings of the payment method.
   * @param stdClass $order
   *   The order object which should be paid for.
   */
  public function __construct($settings) {
    $this->settings = $settings;

    // Validate settings if they have been configured correctly.
    foreach (array('api_clientid', 'api_secret') as $key) {
      if (empty($settings[$key])) {
        return;
      }
    }

    // Initialize the API context.
    $this->apiContext = new ApiContext(
      new OAuthTokenCredential(
        $settings['api_clientid'],
        $settings['api_secret']
      )
    );

    // Configure the API context.
    $this->apiContext->setConfig([
      'mode' => $settings['server'],
      'log.LogEnabled' => $settings['logging']['enabled'],
      'log.FileName' => file_directory_temp() . '/DrupalPayPalPlus.log',
      'log.LogLevel' => $settings['logging']['level'],
      'cache.enabled' => TRUE,
      'cache.FileName' => file_directory_temp() . '/DrupalPayPalPlus.cache',
      'http.headers.PayPal-Partner-Attribution-Id' => 'DrupalbyTojio_Cart_PayPalPluswithECS',
    ]);

    $this->available = TRUE;
  }

  public function available() {
    return $this->available;
  }

  /**
   * Set watchdog entry on exception with additional data info for
   * exceptions thrown by the PayPal library.
   *
   * @param \Exception $exception
   *  The thrown exception.
   * @param null $message
   *  An optional additional message to explain what happened.
   */
  protected function watchdog_set_ex(Exception $exception, $message = NULL) {
    // usual watchdog formatting
    $msg = '%type: !message in %function (line %line of %file)';
    // decode exception information into variables
    require_once DRUPAL_ROOT . '/includes/errors.inc';
    $variables = _drupal_decode_exception($exception);
    // append optional additional message
    if (!empty($message)) {
      $variables['!message'] = $message . ' ' . $variables['!message'];
    }
    // get data from an PayPal library exception
    if (method_exists($exception, 'getData')) {
      $msg .= ' data !data';
      $variables['!data'] = $exception->getData();
    }
    // set watchdog entry
    watchdog('PayPalPlus', $msg, $variables, WATCHDOG_ERROR);
  }

}
