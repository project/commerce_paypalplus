Drupal.commerce_paypalplus = {
  selectedThirdPartyMethod: null,
  selectThirdPartyPaymentMethod: function(data, btnId) {
    if (data.action != 'selectThirdPartyPaymentMethod') {
      return;
    }
    var btn, radios, label, item;
    btn = document.getElementById(btnId);
    // modify continue button to use custom handler for third party payment method
    btn.onclick = Drupal.commerce_paypalplus.doContinue;
    this.selectedThirdPartyMethod = data.thirdPartyPaymentMethod;
    radios = jQuery('#edit-commerce-payment-payment-method .form-type-radio');
    jQuery.each(radios, function(){
      label = jQuery(this).find('label.option').text().trim();
      if (label === Drupal.commerce_paypalplus.selectedThirdPartyMethod) {
        item = jQuery(this).find('input');
        // checking the button and triggering change on the radio button will cause Drupal's
        // AJAX system to update the #payment-details container with the
        // form that belongs to the selected methods
        jQuery(item).attr('checked', 'checked').trigger('change');
      }
      else {
        item = jQuery(this).find('input');
        jQuery(item).removeAttr('checked');
      }
    });
  },
  deselectThirdPartyPaymentMethod: function(data, btnId) {
    var btn;
    btn = document.getElementById(btnId);
    // reset continue button behavior to its default (proceed to paypal)
    btn.onclick = PAYPAL.apps.PPP.doCheckout;
    jQuery('#payment-details').html('');
  },
  doContinue: function() {
    // does nothing for now.
  },
  forcePlacement: function() {
    var childOfPaymentDetails, html;
    if (jQuery('#ppp-placeholder.js-moved').length) {
      return;
    }
    childOfPaymentDetails = jQuery('#payment-details #ppp-placeholder');
    if (childOfPaymentDetails.length) {
      html = jQuery(childOfPaymentDetails)[0].outerHTML;
      jQuery(childOfPaymentDetails).parent().parent().prepend(html);
      jQuery('#ppp-placeholder').addClass('js-moved');
    }
  }
}

jQuery(document).ready(function(){
  // force prepend #ppp-placeholder container so that it does not get overwritten
  // as it is the case with everything within #payment-details when the selected method changes
  Drupal.commerce_paypalplus.forcePlacement();
});